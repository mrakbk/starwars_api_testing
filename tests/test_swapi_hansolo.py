import requests
import pytest


@pytest.fixture()
def hansolo():
    response = requests.get('https://swapi.dev/api/people/?search=han+solo')
    return response


def test_is_hansolo_found(hansolo):
    assert hansolo.status_code == 200


def test_has_json_header(hansolo):
    assert hansolo.headers["Content-type"] == "application/json"


def test_is_hansolo_form_corellia(hansolo):
    bio = hansolo.json()
    assert bio is not None
    assert bio["results"][0]["homeworld"] is not None
    homeworld_url = bio["results"][0]["homeworld"]
    homeworld = requests.get(homeworld_url)
    assert homeworld.status_code == 200
    assert homeworld.headers["Content-type"] == "application/json"
    homeworld_data = homeworld.json()
    assert homeworld_data["name"] == "Corellia"


def test_is_hansolo_in_return_of_the_jedi(hansolo):
    bio = hansolo.json()
    assert bio is not None
    assert bio["results"][0]['films'] is not None
    films = bio["results"][0]['films']
    film = requests.get("https://swapi.dev/api/films/?search=return+of+the+jedi")
    assert film.status_code == 200
    assert film.headers["Content-type"] == "application/json"
    film_data = film.json()
    film_url = film_data["results"][0]["url"]
    assert film_url in films
